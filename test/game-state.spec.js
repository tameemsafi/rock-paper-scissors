import * as constants from './../src/js/constants';
import * as actions from './../src/js/actions/game-state';
import reducer from './../src/js/reducers/game-state';

describe('Game state actions', () => {

	it('should create action to change player choice', () => {
		const choice = constants.CHOICE_PAPER;
		const expectResult = {
			type: constants.CHANGE_PLAYER_CHOICE,
			payload: choice
		};
		const actionResult = actions.changePlayerChoice( choice );
		expect(actionResult).to.deep.equal(expectResult);
	});

	it('should create action to change computer choice', () => {
		const choice = constants.CHOICE_PAPER;
		const expectResult = {
			type: constants.CHANGE_COMPUTER_CHOICE,
			payload: choice
		};
		const actionResult = actions.changeComputerChoice( choice );
		expect(actionResult).to.deep.equal(expectResult);
	});

	it('should create action to change game screen', () => {
		const gameScreen = constants.GAME_OUTCOME_SCREEN;
		const expectResult = {
			type: constants.CHANGE_GAME_SCREEN,
			payload: gameScreen
		};
		const actionResult = actions.changeGameScreen( gameScreen );
		expect(actionResult).to.deep.equal(expectResult);
	});

});


describe('Game State Reducers', () => {

	it('should return initial state', () => {
		const expectedResult = {
			screen: constants.PLAYER_SELECT_SCREEN,
			playerChoice: false,
			computerChoice: false
		};
		let reducerResult = reducer(undefined, {});
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle CHANGE_PLAYER_CHOICE', () => {
		let choice = constants.CHOICE_ROCK;
		const expectedResult = {
			screen: constants.PLAYER_SELECT_SCREEN,
			playerChoice: choice,
			computerChoice: false
		};
		let action = actions.changePlayerChoice( choice );
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle CHANGE_COMPUTER_CHOICE', () => {
		let choice = constants.CHOICE_ROCK;
		const expectedResult = {
			screen: constants.PLAYER_SELECT_SCREEN,
			playerChoice: false,
			computerChoice: choice
		};
		let action = actions.changeComputerChoice( choice );
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle CHANGE_GAME_SCREEN', () => {
		let newScreen = constants.GAME_OUTCOME_SCREEN;
		const expectedResult = {
			screen: newScreen,
			playerChoice: false,
			computerChoice: false
		};
		let action = actions.changeGameScreen( newScreen );
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

});
