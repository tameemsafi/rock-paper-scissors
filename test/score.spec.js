import * as constants from './../src/js/constants';
import * as actions from './../src/js/actions/score';
import reducer from './../src/js/reducers/score';

describe('Score actions', () => {

	it('should create action to increase player wins', () => {
		const expectResult = {
			type: constants.ADD_PLAYER_WIN,
			payload: 0
		};
		const actionResult = actions.addPlayerWin();
		expect(actionResult).to.deep.equal(expectResult);
	});

	it('should create action to increase computer wins', () => {
		const expectResult = {
			type: constants.ADD_COMPUTER_WIN,
			payload: 0
		};
		const actionResult = actions.addComputerWin();
		expect(actionResult).to.deep.equal(expectResult);
	});

	it('should create action to increase games played', () => {
		const expectResult = {
			type: constants.ADD_GAME_PLAYED,
			payload: 0
		};
		const actionResult = actions.addGamePlayed();
		expect(actionResult).to.deep.equal(expectResult);
	});

	it('should create action to increase game ties', () => {
		const expectResult = {
			type: constants.ADD_GAME_TIE,
			payload: 0
		};
		const actionResult = actions.addGameTie();
		expect(actionResult).to.deep.equal(expectResult);
	});


});

describe('Score Reducers', () => {

	it('should return initial state', () => {
		const expectedResult = {
			computerWins: 0,
			playerWins: 0,
			gameTies: 0,
			gamesPlayed: 0
		};
		let reducerResult = reducer(undefined, {});
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle ADD_COMPUTER_WIN', () => {
		const expectedResult = {
			computerWins: 1,
			playerWins: 0,
			gameTies: 0,
			gamesPlayed: 0
		};
		let action = actions.addComputerWin();
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle ADD_PLAYER_WIN', () => {
		const expectedResult = {
			computerWins: 0,
			playerWins: 1,
			gameTies: 0,
			gamesPlayed: 0
		};
		let action = actions.addPlayerWin();
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle ADD_GAME_TIE', () => {
		const expectedResult = {
			computerWins: 0,
			playerWins: 0,
			gameTies: 1,
			gamesPlayed: 0
		};
		let action = actions.addGameTie();
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

	it('should handle ADD_GAME_PLAYED', () => {
		const expectedResult = {
			computerWins: 0,
			playerWins: 0,
			gameTies: 0,
			gamesPlayed: 1
		};
		let action = actions.addGamePlayed();
		let reducerResult = reducer(undefined, action);
		expect(reducerResult).to.deep.equal(expectedResult);
	});

});
