import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import ConnectedGameOutcome, { GameOutcome }  from './../src/js/components/GameOutcome';

import { reducers } from './../src/js/reducers';
import * as actions from './../src/js/actions/game-state';
import * as constants from './../src/js/constants';

describe('Game Outcome Component', () => {

	let store, container;

	beforeEach(() => {
		store = store = createStore(
			reducers
		);
		container = mount(
			<Provider store={store}>
				<ConnectedGameOutcome/>
			</Provider>
		);
	});

	it('should render player select component', () => {
		expect(container.find(GameOutcome).length).to.equal(1);
	});

	it('should display game outcome', () => {

		store.dispatch( actions.changeGameScreen(constants.GAME_OUTCOME_SCREEN) );
		store.dispatch( actions.changePlayerChoice(constants.CHOICE_PAPER) );
		store.dispatch( actions.changeComputerChoice(constants.CHOICE_ROCK) );

		let gameOutcomeComponent = container.find(GameOutcome);
		let leftOutcome = gameOutcomeComponent.find('.game-outcome__left > .game-outcome__item');
		let rightOutcome = gameOutcomeComponent.find('.game-outcome__right > .game-outcome__item');
		let outcomeMessage = gameOutcomeComponent.find('.game-outcome__message');

		expect(leftOutcome.hasClass('game-outcome__item--winner')).to.equal(true);
		expect(leftOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.PAPER_NAME);

		expect(rightOutcome.hasClass('game-outcome__item--winner')).to.equal(false);
		expect(rightOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.ROCK_NAME);

		expect(outcomeMessage.text()).to.equal(constants.PLAYER_WON_MSG);

	});

	it('should display game outcome', () => {

		store.dispatch( actions.changeGameScreen(constants.GAME_OUTCOME_SCREEN) );
		store.dispatch( actions.changePlayerChoice(constants.CHOICE_SCISSORS) );
		store.dispatch( actions.changeComputerChoice(constants.CHOICE_ROCK) );

		let gameOutcomeComponent = container.find(GameOutcome);
		let leftOutcome = gameOutcomeComponent.find('.game-outcome__left > .game-outcome__item');
		let rightOutcome = gameOutcomeComponent.find('.game-outcome__right > .game-outcome__item');
		let outcomeMessage = gameOutcomeComponent.find('.game-outcome__message');

		expect(leftOutcome.hasClass('game-outcome__item--winner')).to.equal(false);
		expect(leftOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.SCISSORS_NAME);

		expect(rightOutcome.hasClass('game-outcome__item--winner')).to.equal(true);
		expect(rightOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.ROCK_NAME);

		expect(outcomeMessage.text()).to.equal(constants.COMPUTER_WON_MSG);

	});

	it('should display game outcome', () => {

		store.dispatch( actions.changeGameScreen(constants.GAME_OUTCOME_SCREEN) );
		store.dispatch( actions.changePlayerChoice(constants.CHOICE_SCISSORS) );
		store.dispatch( actions.changeComputerChoice(constants.CHOICE_SCISSORS) );

		let gameOutcomeComponent = container.find(GameOutcome);
		let leftOutcome = gameOutcomeComponent.find('.game-outcome__left > .game-outcome__item');
		let rightOutcome = gameOutcomeComponent.find('.game-outcome__right > .game-outcome__item');
		let outcomeMessage = gameOutcomeComponent.find('.game-outcome__message');

		expect(leftOutcome.hasClass('game-outcome__item--winner')).to.equal(false);
		expect(leftOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.SCISSORS_NAME);

		expect(rightOutcome.hasClass('game-outcome__item--winner')).to.equal(false);
		expect(rightOutcome.find('.game-outcome__choice-name').text()).to.equal(constants.SCISSORS_NAME);

		expect(outcomeMessage.text()).to.equal(constants.GAME_TIE_MSG);

	});

	it('should reset game when click play again button', () => {

		store.dispatch( actions.changeGameScreen(constants.GAME_OUTCOME_SCREEN) );
		store.dispatch( actions.changePlayerChoice(constants.CHOICE_SCISSORS) );
		store.dispatch( actions.changeComputerChoice(constants.CHOICE_SCISSORS) );

		let state = store.getState();

		expect(state.gameState.screen).to.equal(constants.GAME_OUTCOME_SCREEN);
		expect(state.gameState.playerChoice).to.equal(constants.CHOICE_SCISSORS);
		expect(state.gameState.computerChoice).to.equal(constants.CHOICE_SCISSORS);

		let playAgainBtn = container.find(GameOutcome).find('.game-outcome__play-again-btn').first();
		playAgainBtn.simulate('click');

		state = store.getState();

		expect(state.gameState.screen).to.equal(constants.PLAYER_SELECT_SCREEN);
		expect(state.gameState.playerChoice).to.equal(false);
		expect(state.gameState.computerChoice).to.equal(false);

	});

});
