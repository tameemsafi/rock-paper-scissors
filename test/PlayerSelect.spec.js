import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import ConnectedPlayerSelect, { PlayerSelect }  from './../src/js/components/PlayerSelect';

import { reducers } from './../src/js/reducers';
import * as constants from './../src/js/constants';

describe('Player Select Component', () => {

	let store, container;

	beforeEach(() => {
		store = store = createStore(
			reducers
		);
		container = mount(
			<Provider store={store}>
				<ConnectedPlayerSelect/>
			</Provider>
		);
	});

	it('should render player select component', () => {
		expect(container.find(PlayerSelect).length).to.equal(1);
	});

	it('should display 3 options for rock, paper, scissors', () => {
		let options = container.find(PlayerSelect).find('.player-select__option');
		expect(options.length).to.equal(3);
	});

	it('should update store when clicking rock option', () => {
		let options = container.find(PlayerSelect).find('.player-select__option');
		options.at(0).simulate('click');
		let state = store.getState();
		expect(state.gameState.screen).to.equal(constants.GAME_OUTCOME_SCREEN);
		expect(state.gameState.playerChoice).to.equal(constants.CHOICE_ROCK);
		expect(state.gameState.computerChoice).to.not.equal(false);
	});

	it('should update store when clicking paper option', () => {
		let options = container.find(PlayerSelect).find('.player-select__option');
		options.at(1).simulate('click');
		let state = store.getState();
		expect(state.gameState.screen).to.equal(constants.GAME_OUTCOME_SCREEN);
		expect(state.gameState.playerChoice).to.equal(constants.CHOICE_PAPER);
		expect(state.gameState.computerChoice).to.not.equal(false);
	});

	it('should update store when clicking scissors option', () => {
		let options = container.find(PlayerSelect).find('.player-select__option');
		options.at(2).simulate('click');
		let state = store.getState();
		expect(state.gameState.screen).to.equal(constants.GAME_OUTCOME_SCREEN);
		expect(state.gameState.playerChoice).to.equal(constants.CHOICE_SCISSORS);
		expect(state.gameState.computerChoice).to.not.equal(false);
	});

});
