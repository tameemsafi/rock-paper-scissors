import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import ConnectedScoreBoard, { ScoreBoard }  from './../src/js/components/ScoreBoard';

import * as actions from './../src/js/actions/score';
import { reducers } from './../src/js/reducers';

describe('Score Board Component', () => {

	const initialState = {
		computerWins: 0,
		playerWins: 0,
		gameTies: 0,
		gamesPlayed: 0
	};

	let store, container;

	beforeEach(() => {
		store = store = createStore(
			reducers
		);
		container = mount(
			<Provider store={store}>
				<ConnectedScoreBoard/>
			</Provider>
		);
	});

	it('should render score board component', () => {
		expect(container.length).to.equal(1);
	});

	it('props should match initial state', () => {
		let scoreBoardComponent = container.find(ScoreBoard);
		expect(scoreBoardComponent.props().computerWins).to.equal(initialState.computerWins);
		expect(scoreBoardComponent.props().playerWins).to.equal(initialState.playerWins);
		expect(scoreBoardComponent.props().gameTies).to.equal(initialState.gameTies);
		expect(scoreBoardComponent.props().gamesPlayed).to.equal(initialState.gamesPlayed);
	});

	it('should render initial state', () => {
		let scoreBoardComponent = container.find(ScoreBoard);
		let counts = scoreBoardComponent.find('span.score-board__count');
		expect(counts.at(0).text()).to.equal(initialState.playerWins.toString());
		expect(counts.at(1).text()).to.equal(initialState.computerWins.toString());
		expect(counts.at(2).text()).to.equal(initialState.gamesPlayed.toString());
		expect(counts.at(3).text()).to.equal(initialState.playerWins + '%');
	});

	it('should dispatch actions and update props', () => {

		store.dispatch( actions.addPlayerWin() );
		store.dispatch( actions.addGamePlayed() );
		store.dispatch( actions.addGameTie() );
		store.dispatch( actions.addComputerWin() );

		let scoreBoardComponent = container.find(ScoreBoard);

		expect(scoreBoardComponent.props().playerWins).to.equal(1);
		expect(scoreBoardComponent.props().computerWins).to.equal(1);
		expect(scoreBoardComponent.props().gamesPlayed).to.equal(1);
		expect(scoreBoardComponent.props().gameTies).to.equal(1);

	});

	it('should dispatch actions and render new state', () => {

		store.dispatch( actions.addPlayerWin() );
		store.dispatch( actions.addGamePlayed() );

		let scoreBoardComponent = container.find(ScoreBoard);
		let counts = scoreBoardComponent.find('span.score-board__count');

		expect(counts.at(0).text()).to.equal('1');
		expect(counts.at(1).text()).to.equal('0');
		expect(counts.at(2).text()).to.equal('1');
		expect(counts.at(3).text()).to.equal('100%');

	});

	it('should dispatch 3 computer wins and render new state', () => {

		store.dispatch( actions.addComputerWin() );
		store.dispatch( actions.addComputerWin() );
		store.dispatch( actions.addComputerWin() );

		store.dispatch( actions.addGamePlayed() );
		store.dispatch( actions.addGamePlayed() );
		store.dispatch( actions.addGamePlayed() );

		let scoreBoardComponent = container.find(ScoreBoard);
		let counts = scoreBoardComponent.find('span.score-board__count');

		expect(counts.at(0).text()).to.equal('0');
		expect(counts.at(1).text()).to.equal('3');
		expect(counts.at(2).text()).to.equal('3');
		expect(counts.at(3).text()).to.equal('0%');

	});

});
