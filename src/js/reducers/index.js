import { combineReducers } from 'redux';

import score from './score';
import gameState from './game-state';

export const reducers = combineReducers({
	score,
	gameState
});
