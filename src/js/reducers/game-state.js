import {
	PLAYER_SELECT_SCREEN,
	CHANGE_PLAYER_CHOICE,
	CHANGE_GAME_SCREEN,
	CHANGE_COMPUTER_CHOICE
} from './../constants';

const initialState = {
	screen: PLAYER_SELECT_SCREEN,
	playerChoice: false,
	computerChoice: false
};

export default function(state = initialState, action) {
	switch(action.type) {
		case CHANGE_PLAYER_CHOICE: {
			return { ...state, playerChoice: action.payload };
		}
		case CHANGE_COMPUTER_CHOICE: {
			return { ...state, computerChoice: action.payload };
		}
		case CHANGE_GAME_SCREEN: {
			return { ...state, screen: action.payload };
		}
		default: {
			return state;
		}
	}
}
