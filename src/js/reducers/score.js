import {
	ADD_COMPUTER_WIN,
	ADD_GAME_TIE,
	ADD_PLAYER_WIN,
	ADD_GAME_PLAYED
} from './../constants';

const initialState = {
	computerWins: 0,
	playerWins: 0,
	gameTies: 0,
	gamesPlayed: 0
};

export default function(state = initialState, action) {
	switch(action.type) {
		case ADD_COMPUTER_WIN: {
			return { ...state, computerWins: state.computerWins + 1 };
		}
		case ADD_PLAYER_WIN: {
			return { ...state, playerWins: state.playerWins + 1 };
		}
		case ADD_GAME_TIE: {
			return { ...state, gameTies: state.gameTies + 1 };
		}
		case ADD_GAME_PLAYED: {
			return { ...state, gamesPlayed: state.gamesPlayed + 1 };
		}
		default: {
			return state;
		}
	}
}
