export const ADD_COMPUTER_WIN = 'add_computer_win';
export const ADD_PLAYER_WIN = 'add_player_win';
export const ADD_GAME_TIE = 'add_game_tie';
export const ADD_GAME_PLAYED = 'add_game_played';

export const PLAYER_SELECT_SCREEN = 'player_select_screen';
export const GAME_OUTCOME_SCREEN = 'game_outcome_screen';

export const CHOICE_PAPER = 'choice_paper';
export const CHOICE_ROCK = 'choice_rock';
export const CHOICE_SCISSORS = 'choice_scissors';

export const CHANGE_PLAYER_CHOICE = 'change_player_choice';
export const CHANGE_GAME_SCREEN = 'change_game_screen';
export const CHANGE_COMPUTER_CHOICE = 'change_computer_choice';

export const WINNER_COMPUTER = 'winner_computer';
export const WINNER_PLAYER = 'winner_player';
export const GAME_TIE = 'game_tie';

export const PLAYER_WON_MSG = 'You have won!';
export const COMPUTER_WON_MSG = 'Computer has won!';
export const GAME_TIE_MSG = 'Game is tie!';
export const ERROR_MSG = 'Unexpected error';

export const ROCK_NAME = 'Rock';
export const PAPER_NAME = 'Paper';
export const SCISSORS_NAME = 'Scissors';
