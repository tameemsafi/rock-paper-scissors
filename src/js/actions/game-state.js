import {
	CHANGE_PLAYER_CHOICE,
	CHANGE_GAME_SCREEN,
	CHANGE_COMPUTER_CHOICE,
} from './../constants';

export function changePlayerChoice( payload ) {
	return {
		type: CHANGE_PLAYER_CHOICE,
		payload
	}
}

export function changeComputerChoice( payload ) {
	return {
		type: CHANGE_COMPUTER_CHOICE,
		payload
	}
}

export function changeGameScreen( payload ) {
	return {
		type: CHANGE_GAME_SCREEN,
		payload
	}
}
