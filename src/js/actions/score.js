import {
	ADD_PLAYER_WIN,
	ADD_GAME_TIE,
	ADD_COMPUTER_WIN,
	ADD_GAME_PLAYED
} from './../constants';

export function addComputerWin() {
	return {
		type: ADD_COMPUTER_WIN,
		payload: 0
	}
}

export function addPlayerWin() {
	return {
		type: ADD_PLAYER_WIN,
		payload: 0
	}
}

export function addGameTie() {
	return {
		type: ADD_GAME_TIE,
		payload: 0
	}
}

export function addGamePlayed() {
	return {
		type: ADD_GAME_PLAYED,
		payload: 0
	}
}
