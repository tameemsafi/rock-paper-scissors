import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
	playerWins: state.score.playerWins,
	computerWins: state.score.computerWins,
	gameTies: state.score.gameTies,
	gamesPlayed: state.score.gamesPlayed
});

export class ScoreBoard extends Component {

	/**
	 * Returns the percentage of games player has won as string
	 */
	getWinRatePercentage() {
		let winRatio = this.props.playerWins / this.props.gamesPlayed;
		if( isNaN( winRatio ) ) {
			return '0%';
		}
		let percentage = this.getRoundedToTwoDecimals( winRatio * 100 );
		return percentage + '%';
	}

	/**
	 * Returns number rounded to two decimal places
	 * @param number
	 */
	getRoundedToTwoDecimals( number ) {
		let num = number * 100;
		num = Math.floor( num );
		return num / 100;
	}

	render() {
		return (
			<div className="score-board">

				<div className="score-board__item">

					<span className="score-board__heading">
						Player Wins
					</span>

					<span className="score-board__count">
						{this.props.playerWins}
					</span>

				</div>

				<div className="score-board__item">

					<span className="score-board__heading">
						Computer Wins
					</span>

					<span className="score-board__count">
						{this.props.computerWins}
					</span>

				</div>

				<div className="score-board__item">

					<span className="score-board__heading">
						Games Played
					</span>

					<span className="score-board__count">
						{this.props.gamesPlayed}
					</span>

				</div>

				<div className="score-board__item">

					<span className="score-board__heading">
						Win Rate
					</span>

					<span className="score-board__count">
						{this.getWinRatePercentage()}
					</span>

				</div>

			</div>
		);
	}
}

export default connect(mapStateToProps)(ScoreBoard);
