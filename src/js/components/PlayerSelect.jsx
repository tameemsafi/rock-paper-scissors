import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	CHOICE_ROCK,
	CHOICE_PAPER,
	CHOICE_SCISSORS,
	GAME_OUTCOME_SCREEN
} from './../constants';

import {
	changePlayerChoice,
	changeComputerChoice,
	changeGameScreen
} from './../actions/game-state';

import paperIcon from './../../img/paper.svg';
import scissorsIcon from './../../img/scissors.svg';
import rockIcon from './../../img/rock.svg';

const mapStateToProps = (state) => ({
	gameState: state.gameState
});

export class PlayerSelect extends Component {

	/**
	 * Changes the players choice with selected choice
	 * Changes computers choice to a random choice
	 * Changes screen to show game outcome
	 *
	 * @param choice The players choice (Rock, Paper or Scissors)
	 */
	choiceSelect( choice ) {
		if( choice != CHOICE_ROCK && choice != CHOICE_PAPER && choice != CHOICE_SCISSORS ) {
			return;
		}
		this.props.dispatch( changePlayerChoice( choice ) );
		this.props.dispatch( changeComputerChoice( this.generateComputerChoice() ) );
		this.props.dispatch( changeGameScreen( GAME_OUTCOME_SCREEN ) );
	}

	/**
	 * Generates a random choice for the computer using basic math
	 */
	generateComputerChoice() {
		// Get random number between 1 to 100
		let randomNumber = Math.floor(Math.random() * 100) + 1;
		if( randomNumber <= 35 ) {
			return CHOICE_ROCK;
		} else if(randomNumber <= 70) {
			return CHOICE_SCISSORS;
		}
		return CHOICE_PAPER;
	}

	render() {
		return (
			<div className="player-select">

				<h2 className="player-select__title">
					Select Your Move
				</h2>

				<div className="player-select__wrapper">

					<ul className="player-select__options">

						<li className="player-select__option" onClick={this.choiceSelect.bind(this, CHOICE_ROCK)}>

							<img src={rockIcon} alt="Rock" className="player-select__option-icon"/>

							<span className="player-select__option-name">
								Rock
							</span>

						</li>

						<li className="player-select__option" onClick={this.choiceSelect.bind(this, CHOICE_PAPER)}>

							<img src={paperIcon} alt="Paper" className="player-select__option-icon"/>

							<span className="player-select__option-name">
								Paper
							</span>

						</li>

						<li className="player-select__option" onClick={this.choiceSelect.bind(this, CHOICE_SCISSORS)}>

							<img src={scissorsIcon} alt="Scissors" className="player-select__option-icon"/>

							<span className="player-select__option-name">
								Scissors
							</span>

						</li>

					</ul>

				</div>

			</div>
		);
	}
}

export default connect(mapStateToProps)(PlayerSelect);
