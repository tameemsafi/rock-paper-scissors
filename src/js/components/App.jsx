import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	PLAYER_SELECT_SCREEN,
	GAME_OUTCOME_SCREEN
} from './../constants';

import ScoreBoard from './ScoreBoard';
import PlayerSelect from './PlayerSelect';
import GameOutcome from './GameOutcome';


const mapStateToProps = state => ({
	screen: state.gameState.screen
});

export class App extends Component {

	/**
	 * Returns the current screen based on the state
	 * @returns {*}
	 */
	getScreen() {
		if( this.props.screen == PLAYER_SELECT_SCREEN ) {
			return <PlayerSelect/>;
		} else if( this.props.screen === GAME_OUTCOME_SCREEN ) {
			return <GameOutcome/>;
		}
		return false;
	}

	render() {
		return (
			<div className="app">

				<div className="app__container">

					<ScoreBoard/>

					{this.getScreen()}

				</div>

			</div>
		);
	}
}

export default connect(mapStateToProps)(App);
