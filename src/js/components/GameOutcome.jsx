import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	CHOICE_ROCK,
	CHOICE_PAPER,
	CHOICE_SCISSORS,
	WINNER_COMPUTER,
	WINNER_PLAYER,
	GAME_TIE,
	PLAYER_SELECT_SCREEN,
	PLAYER_WON_MSG,
	COMPUTER_WON_MSG,
	GAME_TIE_MSG,
	ERROR_MSG,
	ROCK_NAME,
	PAPER_NAME,
	SCISSORS_NAME
} from './../constants';

import {
	changeComputerChoice,
	changeGameScreen,
	changePlayerChoice
} from './../actions/game-state';

import {
	addComputerWin,
	addGameTie,
	addPlayerWin,
	addGamePlayed
} from './../actions/score';

import rockIcon from './../../img/rock.svg';
import paperIcon from './../../img/paper.svg';
import scissorsIcon from './../../img/scissors.svg';

const mapStateToProps = (state) => ({
	playerChoice: state.gameState.playerChoice,
	computerChoice: state.gameState.computerChoice
});

export class GameOutcome extends Component {

	/**
	 * Add score once before this component is mounted
	 */
	componentWillMount() {
		let winner = this.getWinner();
		if( winner === WINNER_COMPUTER ) {
			this.props.dispatch( addComputerWin() );
		} else if( winner === WINNER_PLAYER ) {
			this.props.dispatch( addPlayerWin() );
		} else if( winner === GAME_TIE ) {
			this.props.dispatch( addGameTie() );
		}
		this.props.dispatch( addGamePlayed() );
	}

	/**
	 * Gets the information about the choice
	 *
	 * @param choice Selected choice (Rock, Paper, or Scissors)
	 * @returns {*} The icon src and also the name of the choice
	 */
	getChoiceInfo( choice ) {
		if( choice === CHOICE_ROCK ) {
			return {
				icon: rockIcon,
				name: ROCK_NAME
			};
		} else if( choice === CHOICE_PAPER ) {
			return {
				icon: paperIcon,
				name: PAPER_NAME
			};
		} else if( choice === CHOICE_SCISSORS ) {
			return {
				icon: scissorsIcon,
				name: SCISSORS_NAME
			};
		}
		return false;
	}

	/**
	 * Gets the players choice information (icon src and name)
	 * @returns {{icon, name}|*}
	 */
	getPlayerChoice() {
		return this.getChoiceInfo( this.props.playerChoice );
	}

	/**
	 * Gets the computers choice information (icon src and name)
	 * @returns {{icon, name}|*}
	 */
	getComputerChoice() {
		return this.getChoiceInfo( this.props.computerChoice );
	}

	/**
	 * Checks who has won the game based on the Rock, Paper, Scissors rules
	 * - Paper is better than rock
	 * - Scissors is better than paper
	 * - Rock is better than scissors
	 */
	getWinner() {
		if( this.props.playerChoice === this.props.computerChoice ) {
			return GAME_TIE;
		} else if( this.props.playerChoice === CHOICE_ROCK ) {
			if( this.props.computerChoice == CHOICE_PAPER ) {
				return WINNER_COMPUTER;
			} else {
				return WINNER_PLAYER;
			}
		} else if( this.props.playerChoice === CHOICE_PAPER ) {
			if( this.props.computerChoice == CHOICE_SCISSORS ) {
				return WINNER_COMPUTER;
			} else {
				return WINNER_PLAYER;
			}
		} else if( this.props.playerChoice === CHOICE_SCISSORS ) {
			if( this.props.computerChoice == CHOICE_ROCK ) {
				return WINNER_COMPUTER;
			} else {
				return WINNER_PLAYER;
			}
		}
		return GAME_TIE;
	}

	/**
	 * Returns a message based on the outcome of the game
	 * - Tie
	 * - Computer won
	 * - Player won
	 * @returns {*} Game outcome message
	 */
	getMessage() {
		if( this.getWinner() === GAME_TIE ) {
			return GAME_TIE_MSG;
		} else if( this.getWinner() === WINNER_COMPUTER ) {
			return COMPUTER_WON_MSG;
		} else if( this.getWinner() === WINNER_PLAYER ) {
			return PLAYER_WON_MSG;
		}
		return ERROR_MSG;
	}

	/**
	 * Changes screen back to player select
	 * Clears the players and computers choice
	 */
	resetGame() {
		this.props.dispatch( changeGameScreen( PLAYER_SELECT_SCREEN ) );
		this.props.dispatch( changePlayerChoice( false ) );
		this.props.dispatch( changeComputerChoice( false ) );
	}

	render() {
		let gameItemCass = 'game-outcome__item';
		let gameItemWinnerclass = 'game-outcome__item--winner';
		let playerItemClass = (this.getWinner() == WINNER_PLAYER) ? gameItemCass + ' ' + gameItemWinnerclass : gameItemCass;
		let computerItemClass = (this.getWinner() == WINNER_COMPUTER) ? gameItemCass + ' ' + gameItemWinnerclass : gameItemCass;
		return (
			<div className="game-outcome">

				<div className="game-outcome__top">

					<div className="game-outcome__left">

						<div className={playerItemClass}>

							<h2 className="game-outcome__name">
								You
							</h2>

							<div className="game-outcome__choice">

								<img src={this.getPlayerChoice().icon} alt={this.getPlayerChoice().name} className="game-outcome__choice-icon"/>

								<span className="game-outcome__choice-name">
									{this.getPlayerChoice().name}
								</span>

							</div>

						</div>

					</div>

					<div className="game-outcome__right">

						<div className={computerItemClass}>

							<h2 className="game-outcome__name">
								Computer
							</h2>

							<div className="game-outcome__choice">

								<img src={this.getComputerChoice().icon} alt={this.getComputerChoice().name} className="game-outcome__choice-icon"/>

								<span className="game-outcome__choice-name">
								{this.getComputerChoice().name}
								</span>

							</div>

						</div>

					</div>

					<br className="clearfix"/>

				</div>

				<div className="game-outcome__bottom">

					<p className="game-outcome__message">

						{this.getMessage()}

					</p>

					<button className="game-outcome__play-again-btn" onClick={this.resetGame.bind(this)}>
						Play Again
					</button>

				</div>

			</div>
		);
	}
}

export default connect(mapStateToProps)(GameOutcome);
